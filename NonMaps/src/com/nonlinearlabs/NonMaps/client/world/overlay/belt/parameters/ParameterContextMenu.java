package com.nonlinearlabs.NonMaps.client.world.overlay.belt.parameters;

import com.nonlinearlabs.NonMaps.client.world.maps.parameters.Parameter;
import com.nonlinearlabs.NonMaps.client.world.overlay.ContextMenu;
import com.nonlinearlabs.NonMaps.client.world.overlay.OverlayLayout;

public class ParameterContextMenu extends ContextMenu {

	public ParameterContextMenu(OverlayLayout parent, final Parameter param) {
		super(parent);
	}
}
